package sasf.net.sasfsecurity.security;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import sasf.net.sasfsecurity.model.AgeUsuarios;
import sasf.net.sasfsecurity.repository.AgeUsuariosRespository;

@Service
public class UsuarioDetailsServiceImpl implements UserDetailsService {
	
	private AgeUsuariosRespository ageUsuariosRespository;

	public UsuarioDetailsServiceImpl(AgeUsuariosRespository ageUsuariosRespository) {
		this.ageUsuariosRespository = ageUsuariosRespository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AgeUsuarios usuario = ageUsuariosRespository.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(username);
		}
		return new User(usuario.getUsername(), usuario.getPassword(), new ArrayList<>());
	}
}
