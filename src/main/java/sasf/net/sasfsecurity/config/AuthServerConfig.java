package sasf.net.sasfsecurity.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private Environment environment;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private DataSource dataSource;
	
	private static final String MSJ_USUARIO_CLAVE_INCORRECTO = "Usuario o clave incorrecta";

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		System.out.println("Clave : " + new BCryptPasswordEncoder(12).encode("12345"));
		String useJwt = environment.getProperty("spring.security.oauth.jwt");
		if (useJwt != null && "true".equalsIgnoreCase(useJwt.trim())) {
			endpoints.tokenStore(tokenStore())
					.userDetailsService(userDetailsService).tokenEnhancer(jwtConeverter())
					.authenticationManager(authenticationManager)
					.exceptionTranslator(e -> {
		                if (e instanceof InvalidGrantException) {
		                	InvalidGrantException invalidGrantException = (InvalidGrantException) e;
		                    return ResponseEntity
		                            .status(invalidGrantException.getHttpErrorCode())
		                            .body(new InvalidGrantException(MSJ_USUARIO_CLAVE_INCORRECTO));
		                } else {
		                    throw e;
		                }
		            });
		} else {
			endpoints.authenticationManager(authenticationManager)
			.userDetailsService(userDetailsService)
			.exceptionTranslator(e -> {
                if (e instanceof InvalidGrantException) {
                	InvalidGrantException invalidGrantException = (InvalidGrantException) e;
                    return ResponseEntity
                            .status(invalidGrantException.getHttpErrorCode())
                            .body(new InvalidGrantException(MSJ_USUARIO_CLAVE_INCORRECTO));
                } else {
                    throw e;
                }
            });
		}
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security
		.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
		// clients.withClientDetails(clientDetailsService)
		clients.jdbc(dataSource);
		// clients.inMemory();
		/*
		 * clients.inMemory().withClient("10101010").secret("11110000").scopes("admin",
		 * "age") .authorizedGrantTypes("client_credentials", "password",
		 * "refresh_token") .accessTokenValiditySeconds(1800);
		 */
		// clients.inMemory().withClient("10101010").secret("11110000").scopes("admin",
		// "age")
		// .authorizedGrantTypes("client_credentials", "password", "refresh_token")
		// .accessTokenValiditySeconds(1800);
	}

	/*
	 * @Bean public DataSource dataSource() { DriverManagerDataSource dataSource =
	 * new DriverManagerDataSource();
	 * 
	 * dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
	 * dataSource.setUrl(env.getProperty("jdbc.url"));
	 * dataSource.setUsername(env.getProperty("jdbc.user"));
	 * dataSource.setPassword(env.getProperty("jdbc.pass")); return dataSource; }
	 */

	@Bean
	public TokenStore tokenStore() {
		String useJwt = environment.getProperty("spring.security.oauth.jwt");
		if (useJwt != null && "true".equalsIgnoreCase(useJwt.trim())) {
			return new JwtTokenStore(jwtConeverter());
		} else {
			return new InMemoryTokenStore();
		}
	}

	@Bean
	protected JwtAccessTokenConverter jwtConeverter() {
		String pwd = environment.getProperty("spring.security.oauth.jwt.keystore.password");
		String alias = environment.getProperty("spring.security.oauth.jwt.keystore.alias");
		String keystore = environment.getProperty("spring.security.oauth.jwt.keystore.name");

		KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource(keystore),
				pwd.toCharArray());
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setKeyPair(keyStoreKeyFactory.getKeyPair(alias));
		return converter;
	}

}
