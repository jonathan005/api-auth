package sasf.net.sasfsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sasf.net.sasfsecurity.model.AgeUsuarios;
import sasf.net.sasfsecurity.model.AgeUsuariosPK;

public interface AgeUsuariosRespository extends JpaRepository<AgeUsuarios, AgeUsuariosPK> {

	AgeUsuarios findByUsername(String username);
	
	AgeUsuarios findByAgeUsuariosPK(AgeUsuariosPK ageUsuariosPK);
	
	//List<AgeUsuarios> findByAgeUsuariosPKAgeLicencCodigo(int licenciatarioCodigo);
}
