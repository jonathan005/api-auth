package sasf.net.sasfsecurity.model;

import java.io.Serializable;

import javax.persistence.Column;

public class AgeUsuariosPK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "CODIGO")
    private int codigo;
	
	@Column(name = "AGE_LICENC_CODIGO")
    private int ageLicencCodigo;

	public AgeUsuariosPK(int codigo, int ageLicencCodigo) {
		super();
		this.codigo = codigo;
		this.ageLicencCodigo = ageLicencCodigo;
	}

	public AgeUsuariosPK() {
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getAgeLicencCodigo() {
		return ageLicencCodigo;
	}

	public void setAgeLicencCodigo(int ageLicencCodigo) {
		this.ageLicencCodigo = ageLicencCodigo;
	}
	
}
