package sasf.net.sasfsecurity.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 * SUDAMERICANA
 * DE SOFTWARE S.A.
 * 
 * @author  Steven Alvarado
 * @Version 1.0
 */
@Cacheable(false)
@Entity
@Table(name = "AGE_LICENCIATARIOS") 
public class AgeLicenciatarios implements Serializable {

    @Size(max = 13)
    @Column(name = "CONTRIBUYENTE_ESPECIAL_RESOLUC")
    private String contribuyenteEspecialResoluc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "OBLIGADO_LLEVAR_CONTABILIDAD")
    private String obligadoLlevarContabilidad;
    @Size(max = 200)
    @Column(name = "PAGINA_WEB")
    private String paginaWeb;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODIGO")
    private Integer codigo;
    @Size(max = 2000)
    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 2000)
    @Column(name = "NOMBRE_COMERCIAL")
    private String nombreEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 50)
    @Column(name = "NOMBRE_CORTO")
    private String nombreCorto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 200)
    @Column(name = "REPRESENTANTE_LEGAL")
    private String representanteLegal;
    @Transient
    private String tipoIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 30)
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 200)
    @Column(name = "DIRECCION_PRINCIPAL")
    private String direccionPrincipal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 20)
    @Column(name = "TELEFONO1")
    private String telefono1;
    @Size(max = 20)
    @Column(name = "TELEFONO2")
    private String telefono2;
    @Size(max = 200)
    @Column(name = "E_MAIL1")
    private String eMail1;
    @Size(max = 200)
    @Column(name = "E_MAIL2")
    private String eMail2;
    @Size(max = 3)
    @Column(name = "ES_CORPORACION")
    private String esCorporacion;
    @Size(max = 3)
    @Transient
    private String contribuyenteEspecial;
    @Size(max = 2000)
    @Column(name = "OBSERVACION")
    private String observacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ESTADO")
    private String estado;
    /*@Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ESTADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEstado;
    @Size(max = 2000)
    @Column(name = "OBSERVACION_ESTADO")
    private String observacionEstado;
    @Size(max = 2000)
    @Column(name = "RUTA_LICENCIATARIO")
    private String rutaLicenciatario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "USUARIO_INGRESO")
    private long usuarioIngreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "UBICACION_INGRESO")
    private String ubicacionIngreso;
    @Size(max = 20)
    @Column(name = "USUARIO_MODIFICACION")
    private Long usuarioModificacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 200)
    @Column(name = "UBICACION_MODIFICACION")
    private String ubicacionModificacion;*/
    @OneToMany(mappedBy = "ageLicenciatarios")
    private Collection<AgeLicenciatarios> ageLicenciatariosCollection;
    @JoinColumn(name = "AGE_LICENC_CODIGO", referencedColumnName = "CODIGO")
    @ManyToOne
    private AgeLicenciatarios ageLicenciatarios;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "ageLicenciatarios")
    //private Collection<AgeUsuarios> ageUsuariosCollection;
    
    public AgeLicenciatarios() {
    }

    public AgeLicenciatarios(Integer codigo) {
        this.codigo = codigo;
    }

    public AgeLicenciatarios(Integer codigo, String nombreCorto) {
        this.codigo = codigo;
        this.nombreCorto = nombreCorto;
    }

    public AgeLicenciatarios(Integer codigo, String nombreEmpresa, String nombreCorto, String razonSocial, String representanteLegal, String tipoPersona, String tipoIdentificacion, String numeroIdentificacion, String direccionPrincipal, String telefono1, String telefono2, String email1, String email2,String esCorporacion,String esContEspecial, String estado,String observacionEstado) {
        this.codigo = codigo;
        this.nombreEmpresa = nombreEmpresa;
        this.nombreCorto = nombreCorto;
        this.razonSocial = razonSocial;
        this.representanteLegal = representanteLegal;
        //this.tipoPersona = tipoPersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.direccionPrincipal = direccionPrincipal;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.eMail1 = email1;
        this.eMail2 = email2;
        this.esCorporacion = esCorporacion;
        this.contribuyenteEspecial = esContEspecial;
        this.estado = estado;
        //this.observacionEstado = observacionEstado;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /*public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }*/

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getEMail1() {
        return eMail1;
    }

    public void setEMail1(String eMail1) {
        this.eMail1 = eMail1;
    }

    public String getEMail2() {
        return eMail2;
    }

    public void setEMail2(String eMail2) {
        this.eMail2 = eMail2;
    }

    public String getEsCorporacion() {
        return esCorporacion;
    }

    public void setEsCorporacion(String esCorporacion) {
        this.esCorporacion = esCorporacion;
    }

    public String getContribuyenteEspecial() {
        return contribuyenteEspecial;
    }

    public void setContribuyenteEspecial(String contribuyenteEspecial) {
        this.contribuyenteEspecial = contribuyenteEspecial;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /*public Date getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

    public String getObservacionEstado() {
        return observacionEstado;
    }

    public void setObservacionEstado(String observacionEstado) {
        this.observacionEstado = observacionEstado;
    }
    
    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getUbicacionIngreso() {
        return ubicacionIngreso;
    }

    public void setUbicacionIngreso(String ubicacionIngreso) {
        this.ubicacionIngreso = ubicacionIngreso;
    }

    public long getUsuarioIngreso() {
        return usuarioIngreso;
    }

    public void setUsuarioIngreso(long usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }

    public Long getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(Long usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }
    
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUbicacionModificacion() {
        return ubicacionModificacion;
    }

    public void setUbicacionModificacion(String ubicacionModificacion) {
        this.ubicacionModificacion = ubicacionModificacion;
    }*/

    @XmlTransient
    public Collection<AgeLicenciatarios> getAgeLicenciatariosCollection() {
        return ageLicenciatariosCollection;
    }

    public void setAgeLicenciatariosCollection(Collection<AgeLicenciatarios> ageLicenciatariosCollection) {
        this.ageLicenciatariosCollection = ageLicenciatariosCollection;
    }

    public AgeLicenciatarios getAgeLicenciatarios() {
        return ageLicenciatarios;
    }

    public void setAgeLicenciatarios(AgeLicenciatarios ageLicenciatarios) {
        this.ageLicenciatarios = ageLicenciatarios;
    }

    /*public String getRutaLicenciatario() {
        return rutaLicenciatario;
    }

    public void setRutaLicenciatario(String rutaLicenciatario) {
        this.rutaLicenciatario = rutaLicenciatario;
    }*/

    public String geteMail1() {
        return eMail1;
    }

    public void seteMail1(String eMail1) {
        this.eMail1 = eMail1;
    }

    public String geteMail2() {
        return eMail2;
    }

    public void seteMail2(String eMail2) {
        this.eMail2 = eMail2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgeLicenciatarios)) {
            return false;
        }
        AgeLicenciatarios other = (AgeLicenciatarios) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.AgeLicenciatarios[ codigo=" + codigo + " ]";
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    public String getContribuyenteEspecialResoluc() {
        return contribuyenteEspecialResoluc;
    }

    public void setContribuyenteEspecialResoluc(String contribuyenteEspecialResoluc) {
        this.contribuyenteEspecialResoluc = contribuyenteEspecialResoluc;
    }

    public String getObligadoLlevarContabilidad() {
        return obligadoLlevarContabilidad;
    }

    public void setObligadoLlevarContabilidad(String obligadoLlevarContabilidad) {
        this.obligadoLlevarContabilidad = obligadoLlevarContabilidad;
    }
    
}
