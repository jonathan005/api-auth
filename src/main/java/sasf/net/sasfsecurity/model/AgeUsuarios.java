package sasf.net.sasfsecurity.model;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Cacheable(false)
@Entity
@Table(name = "AGE_USUARIOS")
public class AgeUsuarios {

    @EmbeddedId
    protected AgeUsuariosPK ageUsuariosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 20)
    @Column(name = "CODIGO_EXTERNO")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 255)
    @Column(name = "CLAVE")
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "TELEFONO_CELULAR")
    private String telefonoCelular;
    @Transient
    private String tipoIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 20)
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 50)
    @Column(name = "NOMBRES")
    private String nombres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 50)
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Size(max = 200)
    @Column(name = "ARCHIVO_FOTO")
    private String archivoFoto;
    @Column(name = "PRIMER_INGRESO")
    private String primerIngreso;
    @Size(max = 200)
    @Column(name = "MAIL_PRINCIPAL")
    private String mailPrincipal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 0, max = 20)
    @Column(name = "TIPO_USUARIO")
    private String tipoUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ESTADO")
    private String estado;
    /*@Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_ESTADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEstado;
    @Size(max = 2000)
    @Column(name = "OBSERVACION_ESTADO")
    private String observacionEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "USUARIO_INGRESO")
    private long usuarioIngreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "UBICACION_INGRESO")
    private String ubicacionIngreso;
    @Size(max = 20)
    @Column(name = "USUARIO_MODIFICACION")
    private Long usuarioModificacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 200)
    @Column(name = "UBICACION_MODIFICACION")
    private String ubicacionModificacion;*/
    
    
    @JoinColumn(name = "AGE_LICENC_CODIGO", referencedColumnName = "CODIGO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AgeLicenciatarios ageLicenciatarios;
    public AgeUsuarios() {
    }

    public void setAgeUsuariosPK(AgeUsuariosPK ageUsuariosPK) {
        this.ageUsuariosPK = ageUsuariosPK;
    }

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

    /*public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }*/

	//@JsonIgnore
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public AgeUsuarios(AgeUsuariosPK ageUsuariosPK) {
        this.ageUsuariosPK = ageUsuariosPK;
    }

    public AgeUsuariosPK getAgeUsuariosPK() {
        return ageUsuariosPK;
    }

	public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getMailPrincipal() {
        return mailPrincipal;
    }

    public void setMailPrincipal(String mailPrincipal) {
        this.mailPrincipal = mailPrincipal;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /*public Date getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

    public String getObservacionEstado() {
        return observacionEstado;
    }

    public void setObservacionEstado(String observacionEstado) {
        this.observacionEstado = observacionEstado;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getUbicacionIngreso() {
        return ubicacionIngreso;
    }

    public void setUbicacionIngreso(String ubicacionIngreso) {
        this.ubicacionIngreso = ubicacionIngreso;
    }

    public long getUsuarioIngreso() {
        return usuarioIngreso;
    }

    public void setUsuarioIngreso(long usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }

    public Long getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(Long usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUbicacionModificacion() {
        return ubicacionModificacion;
    }

    public void setUbicacionModificacion(String ubicacionModificacion) {
        this.ubicacionModificacion = ubicacionModificacion;
    }*/

    @JsonIgnore
    public AgeLicenciatarios getAgeLicenciatarios() {
        return ageLicenciatarios;
    }

	public void setAgeLicenciatarios(AgeLicenciatarios ageLicenciatarios) {
        this.ageLicenciatarios = ageLicenciatarios;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    public String getArchivoFoto() {
        return archivoFoto;
    }

    public void setArchivoFoto(String archivoFoto) {
        this.archivoFoto = archivoFoto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ageUsuariosPK != null ? ageUsuariosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgeUsuarios)) {
            return false;
        }
        AgeUsuarios other = (AgeUsuarios) object;
        if ((this.ageUsuariosPK == null && other.ageUsuariosPK != null) || (this.ageUsuariosPK != null && !this.ageUsuariosPK.equals(other.ageUsuariosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.util.AgeUsuarios[ ageUsuariosPK=" + ageUsuariosPK + " ]";
    }

    public String getPrimerIngreso() {
        return primerIngreso;
    }

    public void setPrimerIngreso(String primerIngreso) {
        this.primerIngreso = primerIngreso;
    }
}
